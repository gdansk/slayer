//
//  CanAttack.swift
//  Slayer
//
//  Created by jon on 11/7/15.
//  Copyright © 2015 Havron, Jon. All rights reserved.
//

import Foundation

protocol CanAttack {
    var attack : Int { get }
    func CanAttack(other: CanDefend) -> Bool
}

extension CanAttack {
    var attack : Int { return -1 }
    func CanAttack(other: CanDefend) -> Bool {
        return attack > other.defense
    }
}