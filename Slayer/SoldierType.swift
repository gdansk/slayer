//
//  SoldierType.swift
//  Slayer
//
//  Created by jon on 11/7/15.
//  Copyright © 2015 Havron, Jon. All rights reserved.
//

import Foundation

class SoldierType : CanAttack, CanDefend {
    // MARK: - Properties
    var cost    : Int = -1 // Number of peasants to summon one
    var upkeep  : Int = -1 // Gold per turn
    var name    : String = "Unkown"
    var image   : String = "Pawn"
    let id      : Int
    
    var attack : Int {
        return stAttack
    }
    
    var defense : Int {
        return stDefense
    }
    
    // MARK: - Private
    
    private var stAttack  : Int = -1
    private var stDefense : Int = -1
    
    // MARK: - Initializers
    
    init(id: Int, name: String, attack: Int, defense: Int, cost: Int, upkeep: Int, image: String = "Pawn"){
        self.id     = id
        stAttack    = attack
        stDefense   = defense
        self.cost   = cost
        self.upkeep = upkeep
        self.name   = name
        self.image  = image
    }
    
    // MARK: - Static
    
    static let defaultSoldierTypes = [
        SoldierType(id: 0, name: "Peasant",  attack: 1, defense: 1, cost: 10, upkeep: 2),
        SoldierType(id: 1, name: "Bishop", attack: 2, defense: 2, cost: 20, upkeep: 6, image: "Bishop"),
        SoldierType(id: 2, name: "Knight", attack: 3, defense: 3, cost: 30, upkeep: 18, image: "Knight"),
        SoldierType(id: 3, name: "King", attack: 4, defense: 4, cost: 40, upkeep: 54, image: "King"),
    ]
    
    static let peasant = SoldierType.defaultSoldierTypes[0]
    static let bishop  = SoldierType.defaultSoldierTypes[1]
    static let knight  = SoldierType.defaultSoldierTypes[2]
    static let king    = SoldierType.defaultSoldierTypes[3]
}

