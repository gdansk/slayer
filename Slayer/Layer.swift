//
//  Layer.swift
//  Slayer
//
//  Created by jon on 12/16/15.
//  Copyright © 2015 Havron, Jon. All rights reserved.
//

import Foundation
import SpriteKit

struct Layer {
    static let Background   : CGFloat = 00
    static let Tile         : CGFloat = 01
    static let MovementArea : CGFloat = 02
    static let Occupier     : CGFloat = 03
    static let ExitButton   : CGFloat = 94
    static let GUIBack      : CGFloat = 95
    static let EndTurn      : CGFloat = 96
    static let BuyFort      : CGFloat = 97
    static let BuyPawn      : CGFloat = 98
    static let GUI          : CGFloat = 99
}