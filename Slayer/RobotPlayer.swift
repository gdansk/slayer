//
//  RobotPlayer.swift
//  Slayer
//
//  Created by jon on 12/15/15.
//  Copyright © 2015 Havron, Jon. All rights reserved.
//

import Foundation

class RobotPlayer {
    static let safetyFactor = 1.2
    
    static func doTurn(let ts: TileScene, let player: Player) {
        var hutHexes : [Hexagon] = []
        for b in player.blobs {
            if let hut = b.hut {
                hutHexes.append(hut)
            }
        }
        
        while !hutHexes.isEmpty {
            let hutHex = hutHexes.removeFirst()
            var blob = player.getNextBlob(hutHex)
            if blob.count > 1 {
                var myArmy = existingUnits(blob)
                var armyCount = 0
                for soldier in myArmy {
                    armyCount += soldier.count
                }
                //print("\(player.name) has \(armyCount) soldier")
                if armyCount > 0 {
                    deforest(ts, blob: blob)
                    blob = conquer(ts, blob: blob)
                    expand(ts, blob: blob)
                } else {
                    expand(ts, blob: blob)
                    myArmy = existingUnits(blob)
                    deforest(ts, blob: blob)
                    //newBlob = conquer(ts, blob: blob)
                }
                
                fortify(ts, blob: blob)
                print("\(player.name) has \(blob.hut!.gold) gold")
            }
        }
    }
    
    // merge two units
    static func merge(newId: Int, deadId : Int) {
        // I never use this because finding good tiles for upgrades is hard
    }
    
    // AI doesn't hug trees
    // AI chops them
    static func deforest(let ts: TileScene, blob: Blob) {
        var treeTiles : [Hexagon] = []
        for tile in blob.tiles {
            if let doodad = tile.doodad {
                if doodad == DoodadType.tree {
                    treeTiles.append(tile)
                }
            }
        }
        
        let army = existingUnits(blob)
        
        for unit in army[SoldierType.peasant.id] {
            //chop the tree
            if let target = treeTiles.first {
                let lumberjack = ts.unitMap[unit]
                
                if ts.placePiece(target, piece: SoldierType.peasant, blob: blob, animated: false, backupSprite: lumberjack) {
                    print("\tremoved tree at \(target.id)")
                    ts.clearSelection()
                } else {
                    //oh god what the hell happened
                    print("(error)\tcouldn't remove tree at \(target.id)")
                }
                
                target.doodad = nil
                target.soldier = SoldierType.peasant
                ts.hexagons[unit]!.soldier = nil
                ts.unitMap[unit] = nil
                
                treeTiles.removeFirst()
            }
        }
    }
    
    static func targets(let ts: TileScene, let blob: Blob, let piece: SoldierType) -> [Hexagon] {
        var results : [Hexagon] = []
        
        for n in blob.neighbors {
            if n.controller != blob.player && ts.canOverpowerDefense(n, piece: piece) {
                results.append(n)
            }
        }
        
        return results
    }
    
    static func placePiece(let ts: TileScene, let blob: Blob, let to: Hexagon, let from: Hexagon, let piece: SoldierType) {
        let target = ts.unitMap[to.id]
        
        ts.unitMap[from.id]!.position = ts.positionForHex(to.id)
        if let t = target {
            t.removeFromParent()
        }
        ts.unitMap[to.id] = ts.unitMap[from.id]
        ts.unitMap[from.id] = nil
        
        to.soldier = piece
        to.doodad = nil
        to.building = nil
        to.moved = true
        from.soldier = nil
        from.doodad = nil
        from.building = nil
    }
    
    // conquer as best we can
    static func conquer(let ts: TileScene, let blob: Blob) -> Blob {
        ts.clearSelection()
        
        // final ending blob
        var newBlob = blob
        
        var conquests = 0
        for unitType in SoldierType.defaultSoldierTypes {
            // get my current army
            var army = existingUnits(newBlob)
            
            //get soldier's hex ids
            var units = army[unitType.id]
            
            // find conquerable tiles by each unit type
            var conquerTiles = targets(ts, blob: newBlob, piece: unitType)
            
            while !conquerTiles.isEmpty && !units.isEmpty {
                var target = conquerTiles.first!
                let unit = units.first!
                let from = ts.hexagons[unit]!
                
                //reblob
                placePiece(ts, blob: newBlob, to: target, from: from, piece: unitType)
                ts.resolveConquest(unitType, player: newBlob.player, to: &target)
                newBlob = from.blob!
                
                //call conquer again, with the new blob
                conquerTiles.removeFirst()
                units.removeFirst()
                
                conquests++
            }
        }
        
        print("\(newBlob.player.name) has conquered \(conquests) tiles.")
        return newBlob
    }
    
    //buy more soldiers
    static func expand(let ts: TileScene, let blob: Blob) {
        let player = blob.player
        for i in 0...3 {
            let unitType = SoldierType.defaultSoldierTypes[3-i]
            if existingUnits(blob)[unitType.id].count > 2 {
                continue
            }
            //var unitRoom = true //room to place units yet
            while canAfford(unitType.cost, upkeep: unitType.upkeep, blob: blob) {
                //buy one!
                print("\(player.name) can afford a \(unitType.name)")
                for tile in blob.tiles {
                    if tile.isEmpty() {
                        //print("\tPlacing \(unitType.name) with gold: \(blob.hut!.gold)")
                        ts.handSprite.removeFromParent()
                        ts.createSoldierPiece(unitType, animated: false)
                        
                        if ts.placePiece(tile, piece: unitType, blob: blob, animated: false) {
                            ts.clearSelection()
                            assert(ts.unitMap[tile.id] != nil, "Piece not placed successfully")
                            blob.hut!.gold -= unitType.cost
                            break
                        }
                        
                        ts.handSprite.removeFromParent()
                    }
                }
            }
        }
        print("\(player.name) finished expanding army.")
    }
    
    // fortify border provinces
    // AI needs to be guarded
    static func fortify(let ts: TileScene, let blob: Blob) {
        let player = blob.player
        var fortTiles : [Hexagon] = []
        
        // find tiles that would be nice to fortify
        for tile in blob.tiles {
            if tile.isEmpty() {
                var shouldFortify = true
                var enemyCount = 0
                for neighbor in tile.neighbors {
                    if let n = neighbor {
                        if n.controller == player {
                            if let building = n.building { //if any neighboring tile controlled by me has a fort
                                if building == BuildingType.fort { // then we shouldn't bother
                                    shouldFortify = false
                                }
                            }
                        } else {
                            enemyCount++
                        }
                    }
                }
                if enemyCount > 0 && shouldFortify {
                    fortTiles.append(tile)
                }
            }
        }
        
        let margin = SoldierType.bishop.upkeep * 30 //54 by default
        // fortify those tiles if we can afford it
        while blob.hut!.gold > margin && !fortTiles.isEmpty {
            let tile = fortTiles.first!
            ts.handSprite.removeFromParent()

            ts.selectedSoldier = ts.createBuildingPiece(BuildingType.fort, animated: false)
            if ts.placeBuilding(tile, building: BuildingType.fort, blob: blob, animated: false) {
                print("\tplaced fort at hex: \(tile.id)")
                blob.hut!.gold -= BuildingType.fort.cost
                fortTiles.removeFirst()
            }
            
            ts.clearSelection()
            ts.handSprite.removeFromParent()
        }
        print("\(player.name) finished fortifying.")
    }
    
    // returns list of hexagon ids with units in them
    static func existingUnits(let blob: Blob) -> [[Int]] {
        var count = 0
        var results : [[Int]] = [[],[],[],[]]
        for tile in blob.tiles {
            if let soldier = tile.soldier {
                if tile.moved == false {
                    results[soldier.id].append(tile.id)
                    count++
                }
            }
        }
        //print("\(blob.player.name) has \(count) soldier")

        return results
    }
    
    //returns if we can afford
    static func canAfford(cost: Int, upkeep: Int, blob: Blob) -> Bool {
        if let hut = blob.hut {
            //let safeCost = Double(cost) * safetyFactor
            let safeUpkeep = Int(ceil(Double(upkeep) * safetyFactor))
            return hut.gold >= cost && blob.netIncome() >= safeUpkeep
        } else {
            return false
        }
    }
}