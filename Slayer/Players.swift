//
//  Player.swift
//  Slayer
//
//  Created by jon on 11/24/15.
//  Copyright © 2015 Havron, Jon. All rights reserved.
//

import Foundation
import SpriteKit

func ==(lhs: Player, rhs: Player) -> Bool {
    return lhs.id == rhs.id
}

class Player : Equatable {
    let color : SKColor //background color of tiles in players control
    let name  : String
    let id    : Int
    var blobs : [Blob] = []
    
    // MARK - Statistics calculated on turn end
    var gold   : Int = 0 //current gold, updated on turn start
    var tiles  : Int = 0 //number of tiles
    var upkeep : Int = 0 //total upkeep this turn
    var units  : Int = 0 //number of soldiers
    var forts  : Int = 0 //number of forts
    var lost   : Int = 0 //number of lost soldiers
    
    init(id: Int, name: String, color: SKColor){
        self.id = id
        self.name = name
        self.color = color
    }
    
    func getNextBlob(tile: Hexagon) -> Blob {
        for b in blobs {
            if b.tiles.contains(tile) {
                return b
            }
        }
        return blobs.first!
    }
    
    static let All = [
        Player(id: 0, name: "Human",      color: SKColor(red:0.286, green:0.688, blue:0.169, alpha:1.0)),
        Player(id: 1, name: "HAL 9000",   color: SKColor(red:0.337, green:0.267, blue:0.365, alpha:1.0)),
        Player(id: 2, name: "Terminator", color: SKColor(red:0.576, green:0.510, blue:0.455, alpha:1.0)),
        Player(id: 3, name: "Bender",     color: SKColor(red:0.635, green:0.773, blue:0.675, alpha:1.0)),
        Player(id: 4, name: "NS-5",       color: SKColor(red:0.784, green:0.773, blue:0.529, alpha:1.0)),
    ]
    
    static let Default = Player(id: -1, name: "Unknown Player", color: SKColor.blackColor())
    static let Human = Player.All[0]
}