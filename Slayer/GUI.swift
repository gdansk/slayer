//
//  GUI.swift
//  Slayer
//
//  Created by jon on 12/16/15.
//  Copyright © 2015 Havron, Jon. All rights reserved.
//

import Foundation
import SpriteKit

class GUI {
    static func setupButtons(let ts: TileScene) {
        let stdSize = CGSize(width: 64, height: 48)
        let size = ts.size
        let goldLabel = SKLabelNode(fontNamed: "DamascusBold")
        let vaultLabel = SKLabelNode(fontNamed: "DamascusBold")
        let profitLabel = SKLabelNode(fontNamed: "DamascusBold")
        
        let buyPawn = SKSpriteNode(imageNamed: "Pawn")
        let pawnButton = SKSpriteNode(imageNamed: "HexOutline")
        
        pawnButton.position = CGPoint(x: 36, y: size.height - 28)
        pawnButton.size = stdSize
        pawnButton.zPosition = Layer.BuyPawn
        buyPawn.position = CGPoint(x: 36, y: size.height - 26)
        buyPawn.size = CGSize(width: 48, height: 32)
        buyPawn.zPosition = Layer.BuyPawn
        
        let buyFort = SKSpriteNode(imageNamed: "Fort")
        let fortButton = SKSpriteNode(imageNamed: "HexOutline")
        
        fortButton.position = CGPoint(x: 104, y: size.height - 28)
        fortButton.size = stdSize
        fortButton.zPosition = Layer.BuyFort
        buyFort.position = CGPoint(x: 104, y: size.height - 26)
        buyFort.size = CGSize(width: 48, height: 32)
        buyFort.zPosition = Layer.BuyFort
        
        let endTurn = SKSpriteNode(imageNamed: "Hourglass")
        let endButton = SKSpriteNode(imageNamed: "HexOutline")
        
        endButton.position = CGPoint(x: size.width - 36, y: size.height - 28)
        endButton.size = stdSize
        endButton.zPosition = Layer.EndTurn
        endTurn.position = CGPoint(x: size.width - 36, y: size.height - 28)
        endTurn.size = CGSize(width: 32, height: 32)
        endTurn.zPosition = Layer.EndTurn
        
        let background = SKShapeNode(rectOfSize: CGSize(width: size.width * 2, height: 58 * 2))
        background.fillColor = SKColor.whiteColor()
        background.zPosition = Layer.GUIBack
        background.position = CGPoint(x: 0, y: size.height)
        
        let distance = size.width/2
        goldLabel.fontColor = SKColor.blackColor()
        goldLabel.fontSize = 36
        goldLabel.text = "Gold:"
        goldLabel.position = CGPoint(x: distance - goldLabel.frame.width/2, y: size.height - 40)
        goldLabel.zPosition = Layer.GUI
        
        vaultLabel.fontColor = SKColor.blackColor()
        vaultLabel.fontSize = 36
        vaultLabel.text = "000"
        vaultLabel.position = goldLabel.position
        vaultLabel.position.x += goldLabel.frame.width - 4
        vaultLabel.zPosition = Layer.GUI
        
        profitLabel.fontColor = SKColor.blackColor()
        profitLabel.fontSize = 36
        profitLabel.text = "00"
        profitLabel.position = vaultLabel.position
        profitLabel.position.x += vaultLabel.frame.width + 12
        profitLabel.zPosition = Layer.GUI
        
        //exit button
        let exitButton = SKSpriteNode(imageNamed: "HexOutline")
        let exitIcon   = SKSpriteNode(imageNamed: "Grave")
        exitButton.position = CGPoint(x: 36, y: 28)
        exitButton.size = stdSize
        exitButton.zPosition = Layer.ExitButton
        exitIcon.position = CGPoint(x: 36, y: 26)
        exitIcon.size = CGSize(width: 48, height: 32)
        exitIcon.zPosition = Layer.ExitButton
        
        ts.rootGUINode.addChild(background)
        ts.rootGUINode.addChild(goldLabel)
        ts.rootGUINode.addChild(vaultLabel)
        ts.rootGUINode.addChild(profitLabel)
        ts.profitLabel = profitLabel
        ts.vaultLabel = vaultLabel
        ts.goldLabel = goldLabel
        
        ts.rootGUINode.addChild(buyPawn)
        ts.rootGUINode.addChild(pawnButton)
        ts.rootGUINode.addChild(buyFort)
        ts.rootGUINode.addChild(fortButton)
        ts.rootGUINode.addChild(endTurn)
        ts.rootGUINode.addChild(endButton)
        
        ts.rootGUINode.addChild(exitButton)
        ts.rootGUINode.addChild(exitIcon)
        
        ts.addChild(ts.rootGUINode)
    }
}