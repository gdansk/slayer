//
//  DoodadType.swift
//  Slayer
//
//  Created by jon on 11/24/15.
//  Copyright © 2015 Havron, Jon. All rights reserved.
//

import Foundation

func ==(lhs: DoodadType, rhs: DoodadType) -> Bool {
    return lhs.id == rhs.id
}

class DoodadType {
    // MARK: - Properties
    var name   : String = "Unknown Doodad"
    var upkeep : Int = -1
    var image  : String = "Tree"
    let id     : Int
    var onControllerTurnStart : (Hexagon) -> Void = { (tile: Hexagon) -> Void in }
    
    // MARK: - Initializers
    init(id: Int, name: String, upkeep: Int, image: String = "Tree"){
        self.name = name
        self.upkeep = upkeep
        self.image = image
        self.id = id
    }
    
    // MARK: - Static
    static let defaultDoodadTypes = [
        DoodadType(id: 0, name: "Pine Tree", upkeep: -1, image: "CTree"), //coniferous tree
        DoodadType(id: 1, name: "Tree", upkeep: -1, image: "Tree"),
        DoodadType(id: 2, name: "Grave", upkeep: 0, image: "Grave")
    ]
    
    static let tree = DoodadType.defaultDoodadTypes[1]
    static let grave = DoodadType.defaultDoodadTypes[2]
}