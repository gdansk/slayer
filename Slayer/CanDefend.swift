//
//  CanDefend.swift
//  Slayer
//
//  Created by jon on 11/7/15.
//  Copyright © 2015 Havron, Jon. All rights reserved.
//

import Foundation

protocol CanDefend {
    var defense : Int { get }
    var cost  : Int { get }
}

extension CanDefend {
    var defense : Int { return -1 }
}