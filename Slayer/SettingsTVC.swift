//
//  SettingsTVC.swift
//  Slayer
//
//  Created by jon on 12/17/15.
//  Copyright © 2015 Havron, Jon. All rights reserved.
//

import Foundation
import UIKit

class SettingsTVC : UITableViewController {
    @IBOutlet weak var soundSwitch     : UISwitch!
    @IBOutlet weak var animationSwitch : UISwitch!
    let defaults = NSUserDefaults.standardUserDefaults()
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        animationSwitch.on = defaults.boolForKey(AnimationKey)
        soundSwitch.on     = defaults.boolForKey(SoundKey)
    }
    
    override func viewWillDisappear(animated: Bool) {
        print("Storing defaults: [anim: \(animationSwitch.on) sound: \(soundSwitch.on)]")
        defaults.setBool(animationSwitch.on, forKey: AnimationKey)
        defaults.setBool(soundSwitch.on,     forKey: SoundKey)
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
}