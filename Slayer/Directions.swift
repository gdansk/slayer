//
//  Directions.swift
//  Slayer
//
//  Created by jon on 11/24/15.
//  Copyright © 2015 Havron, Jon. All rights reserved.
//

import Foundation

struct Direction {
    static let NW = 0
    static let N  = 1
    static let NE = 2
    static let SE = 3
    static let S  = 4
    static let SW = 5
    static let all = [NW, N, NE, SE, S, SW]
}