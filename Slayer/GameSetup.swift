//
//  GameSetup.swift
//  Slayer
//
//  Created by Havron, Jonathan Michael on 12/8/15.
//  Copyright © 2015 Havron, Jon. All rights reserved.
//

import Foundation
import SpriteKit

class GameSetup {
    //constants
    static var firstHex : Hexagon?
    static let treeSpreadChance = 7
    
    static func getNeighbors(let tiles: [Hexagon?], index: Int, mapWidth: Int, mapHeight: Int) -> [Hexagon?] {
        let max = mapHeight * mapWidth
        var neighbors : [Hexagon?] = []
        var offset = 0
        if (index / mapWidth) % 2 == 0 {
            offset = 2
        }
        //example: id = 31, mapWidth = 9
        //NW, N , NE, SE, S , SW
        //40  49  41  23  13  22
        //NW = id + MW
        //N = id + 2*MW
        //NE = id + MW + 1
        //SE = id - MW + 1
        //S = id - 2*MW
        //SW = id - MW
        let indices = [index + mapWidth,     //nw
                       index + 2 * mapWidth, //n
                       index + mapWidth + 1 - offset, //ne
                       index - mapWidth + 1 - offset, //se
                       index - 2 * mapWidth, //s
                       index - mapWidth]     //sw
        for i in indices {
            let ni = i
            if ni > 0 && ni < max {
                neighbors.append(tiles[ni])
            } else {
                neighbors.append(nil)
            }
        }
        return neighbors
    }
    
    static func linkNeighbors(inout tiles: [Hexagon?], mapWidth: Int, mapHeight: Int) {
        for hex in tiles {
            if let actualHex = hex {
                if firstHex == nil { firstHex = actualHex }
                actualHex.neighbors = getNeighbors(tiles, index: actualHex.id, mapWidth: mapWidth, mapHeight: mapHeight)
            }
        }
    }
    
    //expand blob from given tile
    static func expandBlob(inout unvisited: Set<Hexagon>, inout blob: Blob, inout tile: Hexagon) {
        if unvisited.contains(tile) && tile.controller == blob.player {
            blob.addTile(tile)
            tile.blob = blob
            if let building = tile.building {
                if building == BuildingType.hut {
                    if blob.hut != nil {
                        blob.hut!.gold += tile.gold
                        tile.gold = 0
                        tile.building = nil
                    } else {
                        blob.hut = tile
                        print("Found hut for blob #\(blob.id) with gold \(blob.hut!.gold)")
                    }
                }
            }
            unvisited.remove(tile)
            for n in tile.neighbors {
                if var neighbor = n {
                    expandBlob(&unvisited, blob: &blob, tile: &neighbor)
                }
            }
        }
        if tile.controller != blob.player {
            blob.addNeighbor(tile)
        }
    }
    
    static func createBlobs(let hexMap : [Hexagon?]) -> Int {
        for player in Player.All {
            player.blobs.removeAll(keepCapacity: true)
        }
        
        var unvisitedTiles = Set<Hexagon>()
        for hex in hexMap {
            if let h = hex {
                unvisitedTiles.insert(h)
            }
        }
        
        var numBlobs = 0
        while(!unvisitedTiles.isEmpty) {
            let first = unvisitedTiles.first
            //print("Starting at tile \(first?.id)")
            if var currentTile = first {
                var blob = Blob(player: currentTile.controller)
                numBlobs++
                currentTile.controller.blobs.append(blob)
                expandBlob(&unvisitedTiles, blob: &blob, tile: &currentTile)
                print("Create Blob(player: \(blob.player.name), count: \(blob.tiles.count))")
            }
        }
        
        return numBlobs
    }
    
    static func placeHuts(ts : TileScene) {
        for p in Player.All {
            for blob in p.blobs {
                if blob.count > 1 && blob.hut == nil {
                blob.placeHut()
                    if let hut = blob.hut {
                        hut.building = BuildingType.hut
                        //print("Placing hut")
                        hut.gold = 10
                        ts.makeSprite(hut, texture: ts.buildingTextures[BuildingType.hut.image]!)
                    }
                }
            }
        }
    }
    
    static func placeTrees(ts: TileScene) {
        //expand the tree
        DoodadType.tree.onControllerTurnStart = { (tile: Hexagon) -> Void in
            for neighbor in tile.neighbors {
                if let n = neighbor {
                    let chance = GameSetup.treeSpreadChance
                    let roll = 1 + uniformRandom(100)
                    if chance >= roll {
                        if n.isEmpty() {
                            n.doodad = DoodadType.tree
                            ts.makeSprite(n, texture: ts.treeTexture)
                        }
                    }
                }
            }
        }
        
        //transform to tree
        DoodadType.grave.onControllerTurnStart = { (tile: Hexagon) -> Void in
            tile.doodad = DoodadType.tree
            ts.unitMap[tile.id]?.texture = ts.treeTexture
        }
        
        for p in Player.All {
            for blob in p.blobs {
                if blob.count > 1 {
                    for t in blob.tiles {
                        if t.building == nil && t.soldier == nil {
                            let roll = 1 + uniformRandom(100)
                            if GameSetup.treeSpreadChance >= roll {
                                t.doodad = DoodadType.tree
                                ts.makeSprite(t, texture: ts.treeTexture)
                            }
                        }
                    }
                }
            }
        }
    }
}
