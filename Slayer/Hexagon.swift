//
//  Hexagon.swift
//  Slayer
//
//  Created by jon on 11/24/15.
//  Copyright © 2015 Havron, Jon. All rights reserved.
//

import Foundation
import SpriteKit

func ==(lhs: Hexagon, rhs: Hexagon) -> Bool {
    return lhs.id == rhs.id
}

class Hexagon : Hashable, Equatable {
    static let baseIncome = 1
    let id         : Int
    let background   : SKSpriteNode // sprite for the background (should never change anything but color when conquered)
    var neighbors  : [Hexagon?]
    var doodad     : DoodadType? //tree or grave atop the tile
    var building   : BuildingType? //
    var soldier    : SoldierType?
    var controller = Player.Default
    var blob       : Blob?
    var moved      = false //moved this turn?
    var scale      : CGFloat
    var scaleChange : CGFloat = 0.01
    var gold       = 0
    
    init(id: Int, let background: SKSpriteNode){
        self.id = id
        self.background = background //should change color based on controller
        self.neighbors = []
        self.scale = 1
        for _ in Direction.all {
            self.neighbors.append(nil)
        }
    }
    
    func isEmpty() -> Bool {
        return building == nil && doodad == nil && soldier == nil
    }
    
    func getIncome() -> Int {
        var income = Hexagon.baseIncome
        if let myDoodad = doodad {
            income += myDoodad.upkeep
        }
        return income
    }
    
    func getDefense() -> Int {
        if let b = building {
            return b.defense
        } else if let s = soldier {
            return s.defense
        } else {
            return 0
        }
    }
    
    var hashValue : Int {
        get {
            return id
        }
    }
}