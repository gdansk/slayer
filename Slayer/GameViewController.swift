//
//  GameViewController.swift
//  Slayer
//
//  Created by jon on 11/7/15.
//  Copyright (c) 2015 Havron, Jon. All rights reserved.
//

import UIKit
import SpriteKit

class GameViewController: UIViewController {
    var tileScene : TileScene?
    
    override func viewWillAppear(animated: Bool) {
        navigationController?.navigationBarHidden = true
    }
    
    override func viewDidLoad() {
        // Detect the screensize
        let sizeRect = UIScreen.mainScreen().bounds
        let width = sizeRect.size.width * UIScreen.mainScreen().scale
        let height = sizeRect.size.height * UIScreen.mainScreen().scale
        tileScene = TileScene(size: CGSizeMake(width, height))

        super.viewDidLoad()
        
        // Scene should be shown in fullscreen mode
        tileScene!.presenter = self
        
        // Configure the view.
        let skView = self.view as! SKView
        skView.showsFPS = true
        skView.showsNodeCount = true
        
        /* Sprite Kit applies additional optimizations to improve rendering performance */
        skView.ignoresSiblingOrder = true
        
        /* Set the scale mode to scale to fit the window */
        tileScene!.scaleMode = .AspectFill
        
        skView.presentScene(tileScene!)
    }
    
    @IBAction func Panner(recognizer: UIPanGestureRecognizer) {
        let tiles = tileScene!.rootTileNode
        let units = tileScene!.rootUnitNode
        var translation = recognizer.translationInView(tileScene!.view)
        translation.x *= tiles.xScale
        translation.y *= tiles.yScale
        recognizer.setTranslation(CGPointZero, inView: tileScene!.view)
        tiles.runAction(SKAction.moveByX(translation.x, y: translation.y, duration: 0))
        units.runAction(SKAction.moveByX(translation.x, y: translation.y, duration: 0))
        //print("pan regcognized")
    }
    
    @IBAction func Zoomer(recognizer: UIPinchGestureRecognizer) {
        var scale = recognizer.scale
        let tiles = tileScene!.rootTileNode
        let units = tileScene!.rootUnitNode
        if scale > 2 {
            scale = 2
        } else if scale < 0.5 {
            scale = 0.5
        }
        tiles.runAction(SKAction.scaleTo(scale, duration: 0))
        units.runAction(SKAction.scaleTo(scale, duration: 0))
        //print("zoom regcognized with scale \(scale)")
    }

    override func shouldAutorotate() -> Bool {
        return false
    }

    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            return .AllButUpsideDown
        } else {
            return .All
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    override func prefersStatusBarHidden() -> Bool {
        return true
    }
}
