//
//  Utils.swift
//  Slayer
//
//  Created by Havron, Jonathan Michael on 12/8/15.
//  Copyright © 2015 Havron, Jon. All rights reserved.
//

import Foundation

func uniformRandom(max: Int) -> Int {
    return Int(rand() % Int32(max))
    //return Int(arc4random_uniform(UInt32(max)))
}

let SoundKey = "sound"
let AnimationKey = "animation"