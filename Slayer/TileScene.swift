//
//  GameScene.swift
//  Slayer
//
//  Created by jon on 11/7/15.
//  Copyright (c) 2015 Havron, Jon. All rights reserved.
//

import SpriteKit
import AVFoundation

class TileScene: SKScene {
    // Mark - Textures.
    let hexTexture   = SKTexture(imageNamed: "Hexagon")
    let graveTexture = SKTexture(imageNamed: "Grave")
    let treeTexture  = SKTexture(imageNamed: "Tree")
    let soldierTextures = [
        "Pawn"   : SKTexture(imageNamed: "Pawn"),
        "Bishop" : SKTexture(imageNamed: "Bishop"),
        "Knight" : SKTexture(imageNamed: "Knight"),
        "King"   : SKTexture(imageNamed: "King")
    ]
    let buildingTextures = [
        "Hut"  : SKTexture(imageNamed: "Hut"),
        "Fort" : SKTexture(imageNamed: "Fort")
    ]
    
    let map : MapData
    
    // Mark - Map appearance
    //for updating colors
    var hexMap : [SKSpriteNode?] = []
    //for updating units
    var unitMap : [SKSpriteNode?] = []
    //for panning & zooming
    var rootTileNode : SKNode = SKNode()
    var rootUnitNode : SKNode = SKNode()
    var rootGUINode  : SKNode = SKNode()
    
    //for debug selection
    var touchedHex : SKSpriteNode?
    
    // Mark - Selection
    //for selected unit 
    var handSprite = SKSpriteNode(imageNamed: "Hand")
    var selectedUnit = false
    var selectedSoldier : CanDefend?
    var selectedSprite  : SKSpriteNode?
    var selectedBlob    : Blob?
    
    // Mark - UI 
    var goldLabel = SKLabelNode(fontNamed: "DamascusBold")
    var vaultLabel = SKLabelNode(fontNamed: "DamascusBold")
    var profitLabel = SKLabelNode(fontNamed: "DamascusBold")
    var presenter : UIViewController?
    
    var gameOverShown = false
    
    // Mark - Sound.
    var errorSound : AVAudioPlayer?
    
    //adjacency matrix
    var hexagons : [Hexagon?] = []
    
    override init(size: CGSize) {
        map = MapData()
        super.init(size: size)
        map.addHexagons(self)
        GUI.setupButtons(self)
        
        let path = NSBundle.mainBundle().pathForResource("Error", ofType: "wav")
        let url = NSURL.fileURLWithPath(path!)
        do {
            try errorSound = AVAudioPlayer(contentsOfURL: url)
        } catch {
            print("No file at \(url)")
        }
        
        let center = CGPoint(x: -(map.maxX - map.minX)/2, y: 120 - map.minY)
        rootTileNode.position = center
        rootUnitNode.position = center
    }
    
    var animationsOn = NSUserDefaults.standardUserDefaults().boolForKey(AnimationKey)
    var soundOn = NSUserDefaults.standardUserDefaults().boolForKey(SoundKey)

    // when is this called?
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func shadeTiles(let e: Set<Hexagon>, let color: SKColor? = nil) {
        for n in e {
            if color != nil {
                n.background.color = color!
                n.background.colorBlendFactor = 0.7
            } else {
                n.background.color = n.controller.color
                n.background.colorBlendFactor = 1.0
            }
        }
    }
    
    func positionForHex(id: Int) -> CGPoint {
        if let hex = hexMap[id] {
            print("Returning position for tile #\(id)")
            var position = hex.position
            position.y -= 4
            return position
        }
        return CGPoint(x: 0,y: 0)
    }
    
    func holdUnit(sprite: SKSpriteNode, animated: Bool = false) {
        sprite.color = SKColor.redColor()
        let x = size.width / 2 + 24 // center
        let y = size.height - 90 //32 - 58 beneat the menu bar
        if !animated {
            sprite.position.x = x
            sprite.position.y = y
        } else {
            sprite.runAction(SKAction.moveTo(CGPoint(x: x, y: y), duration: 0.1))
        }
        
        handSprite.size = CGSize(width: 64, height: 48)
        //handSprite.position = sprite.position //CGPoint(x: x, y: y)
        if !animated {
            handSprite.position.x = sprite.position.x - 8
            handSprite.position.y = sprite.position.y - 40
        } else {
            handSprite.runAction(SKAction.moveTo(CGPoint(x: x-8, y: y-40), duration: 0.1))
        }
        handSprite.zPosition = 2
        
        addChild(sprite)
        addChild(handSprite)
        
        selectedSprite = sprite
        //selectedUnit = true
    }
    
    func createSoldierPiece(piece: SoldierType, animated: Bool = false) -> SoldierType {
        if let soldier = selectedSoldier {
            let defense = soldier.defense + piece.defense
            if defense > 4 {
                return selectedSoldier as! SoldierType
            }
            print("upgrading held piece to defense \(defense)")
            let type = SoldierType.defaultSoldierTypes[defense-1]
            selectedSprite?.texture = soldierTextures[type.image]
            return type
        } else {
            let sprite = SKSpriteNode(texture: soldierTextures[piece.image])
            sprite.size = CGSize(width: 48, height: 32)
            sprite.position = CGPoint(x: 36, y: size.height - 26)
            holdUnit(sprite, animated: animated)
        }
        return piece
    }
    
    func createBuildingPiece(piece: BuildingType, animated: Bool = false) -> BuildingType {
        let sprite = SKSpriteNode(texture: buildingTextures[piece.image])
        sprite.size = CGSize(width: 48, height: 32)
        sprite.position = CGPoint(x: 104, y: size.height - 26)
        holdUnit(sprite, animated: animated)
        return piece
    }
    
    func buyPawnPressed() {
        let cost = SoldierType.peasant.cost
        if let blob = selectedBlob {
            if let hut = blob.hut {
                if hut.gold >= cost && (selectedSoldier == nil || selectedSoldier as? SoldierType != nil) {
                    print("Bought pawn")
                    selectedSoldier = createSoldierPiece(SoldierType.peasant, animated: animationsOn)
                    selectedUnit = true
                    touchedHex = nil
                    hut.gold -= cost
                }
            }
        }
    }
    
    func buyFortressPressed() {
        let cost = BuildingType.fort.cost
        if let blob = selectedBlob {
            if let hut = blob.hut {
                if hut.gold >= cost && selectedSoldier == nil {
                    print("Pressued buy fort")
                    selectedSoldier = createBuildingPiece(BuildingType.fort, animated: animationsOn)
                    selectedUnit = true
                    touchedHex = nil
                    hut.gold -= cost
                }
            }
        }
    }
    
    func upkeepIncomeAndStarve(player: Player) {
        for b in player.blobs {
            if let hut = b.hut {
                hut.gold += b.netIncome()
                if hut.gold < 0 {
                    hut.gold = 0
                    for t in b.tiles {
                        if t.soldier != nil {
                            t.soldier = nil
                            t.doodad = DoodadType.grave
                            unitMap[t.id]?.texture = graveTexture
                        }
                    }
                }
            }
        }
    }
    
    func endTurnPressed() {
        print("Pressed end turn")
        
        if let blob = selectedBlob {
            if let hut = blob.hut {
                if let piece = selectedSoldier {
                    hut.gold += piece.cost
                }
            }
        }
        selectedSprite?.removeFromParent()
        clearSelection()
        selectedBlob = nil
        if let currentHex = touchedHex {
            let hexId = currentHex.userData!["id"] as! Int
            selectedBlob = hexagons[hexId]?.blob
        }
        touchedHex = nil
        
        // go through each Robot
        for i in 1..<Player.All.count {
            let player = Player.All[i]
            // check the players income, upkeep, and starvation
            upkeepIncomeAndStarve(player)
            // let the robot player go
            if player.blobs.count > 0 {
                RobotPlayer.doTurn(self, player: player)
                handSprite.removeFromParent()
            }
        }
        
        // reset all hex's moved
        for hex in hexagons {
            if let h = hex {
                h.moved = false
                h.scale = 1.0
                h.scaleChange = 0.01
                if let doodad = h.doodad {
                    doodad.onControllerTurnStart(h)
                }
            }
        }
        
        // then human's income
        // human's starvation
        upkeepIncomeAndStarve(Player.Human)
        
        //then return to player for input
    }
    
    func makeSprite(hex: Hexagon, texture: SKTexture) -> SKSpriteNode {
        let sprite = SKSpriteNode(texture: texture)
        
        sprite.size = CGSize(width: 48, height: 32)
        sprite.position = hex.background.position
        sprite.position.y = sprite.position.y + 4
        sprite.zPosition = Layer.Occupier
        
        unitMap[hex.id] = sprite
        rootUnitNode.addChild(sprite)
        
        return sprite
    }
    
    func returnToMain() {
        let navigationController = presenter!.navigationController!

        navigationController.navigationBarHidden = false
        navigationController.popToRootViewControllerAnimated(true)
    }
    
    // sets the destination tile's properties accordingly
    func resolveConquest(piece: SoldierType, let player: Player, inout to: Hexagon) {
        //set gold to 0
        to.gold = 0
        
        //remove unit or building from tile
        to.building = nil
        to.soldier = piece
        to.moved = true
        
        //set controller
        to.controller = player
        
        //re-enumerate blobs
        let numBlobs = GameSetup.createBlobs(hexagons)
        
        // set selected blob to the newly conquered land
        selectedBlob = to.blob

        // if any blob > 1 is without a hut, give it a hut
        for p in Player.All {
            for b in p.blobs {
                if b.count > 1 && b.hut == nil {
                    b.placeHut()
                    //b.hut = hut
                    if b.hut != nil {
                        makeSprite(b.hut!, texture: buildingTextures[BuildingType.hut.image]!)
                        print("Placing hut for blob #\(b.id)")
                    }
                }
                if b.count == 1 && !b.tiles.first!.isEmpty() {
                    let tile = b.tiles.first!
                    tile.doodad = DoodadType.tree
                    tile.building = nil
                    tile.soldier = nil
                    unitMap[tile.id]?.texture = treeTexture
                }
            }
        }
        
        // If game is over, show a pop up with return sending back to the main menu
        let humanBlobs = Player.Human.blobs.count
        if humanBlobs < 1 || numBlobs == 1 {
            if !gameOverShown {
                let navigationController = self.presenter!.navigationController!
                let returnToMain = { (alert: UIAlertAction) in
                    navigationController.navigationBarHidden = false
                    navigationController.popToRootViewControllerAnimated(true)
                }
                
                let message = humanBlobs < 1 ? "You lost" : "You won"
                let gameOverAlert = UIAlertController(title: "Game Over", message: message, preferredStyle: .Alert)
                gameOverAlert.addAction(UIAlertAction(title: "Main Menu", style: .Default, handler: returnToMain))
                
                navigationController.presentViewController(gameOverAlert, animated: true, completion: nil)
            }
            gameOverShown = true
        }
        
        // Recolor the hexs
        for h in hexagons {
            if let hex = h {
                if hex.background.color != hex.controller.color {
                    //print("Changing color of hex #\(hex.id)")
                    hex.background.colorBlendFactor = 1.0
                    hex.background.color = hex.controller.color
                }
                if  hex.isEmpty() && unitMap[hex.id] != nil {
                    unitMap[hex.id]!.removeFromParent()
                    unitMap[hex.id] = nil
                }
            }
        }
    }
    
    func canOverpowerDefense(let hex: Hexagon, let piece : SoldierType) -> Bool {
        if let s = hex.soldier { //it is occupied by a unit stronger than me
            if s.defense >= piece.attack {
                return false
            }
        }
        if let b = hex.building {
            if b.defense >= piece.attack {
                return false
            }
        }
        for n in hex.neighbors { //it has a friendly neighbor stronger than me
            if let neighbor = n {
                if neighbor.controller == hex.controller {
                    if let soldier = neighbor.soldier {
                        if soldier.defense >= piece.attack {
                            return false
                        }
                    }
                    if let building = neighbor.building {
                        if building.defense >= piece.attack {
                            return false
                        }
                    }
                }
            }
        }
        return true
    }
    
    //probably should have used polymorphism
    func placeBuilding(let hex: Hexagon, let building: BuildingType, let blob: Blob,
        animated: Bool, let backupSprite : SKSpriteNode? = nil) -> Bool {
        if !blob.contains(hex) {
            print("Unable to place fort. Outside of domain")
            return false
        }
        if hex.controller != blob.player {
            print("Unable to place fort. Foreign tile")
            return false
        }
        if !hex.isEmpty() {
            print ("Unable to place fort. Already Occupied.")
            return false
        }
        if let _ = selectedSoldier as? BuildingType {
            let anySprite = selectedSprite == nil ? backupSprite : selectedSprite
            if let sprite = anySprite {
                hex.building = building
                print("Placing \(building.name) at hex: \(hex.id)")
                if !animated {
                    sprite.position = hex.background.position
                    sprite.position.y = sprite.position.y + 4
                } else {
                    sprite.runAction(SKAction.moveTo(
                        CGPoint(x: hex.background.position.x, y: hex.background.position.y + 4),
                        duration: 0.1))
                }
                sprite.zPosition = Layer.Occupier
                sprite.size = CGSize(width: 48, height: 32)
                
                unitMap[hex.id] = sprite
                sprite.removeFromParent()
                rootUnitNode.addChild(sprite)
                
                return true
            }
        }
        print("No selected building")
        return false
    }
    
    func placePiece(var hex: Hexagon, let piece: SoldierType, let blob: Blob,
        animated: Bool, let backupSprite : SKSpriteNode? = nil) -> Bool {
        //let player = Player.All[0]
        let player = blob.player
        if !blob.tiles.contains(hex) && !blob.neighbors.contains(hex) {
            print("Hex #\(hex.id) is too far away")
            return false
        }
        //check if any neighbors of tile have defender
        if hex.controller != player && !canOverpowerDefense(hex, piece: piece) {
            print("That hex is guarded by a piece")
            return false
        }
        if hex.controller == player && hex.building != nil {
            return false
        }
        var csprite = selectedSprite
        if csprite == nil {
            print("This should never be called")
            csprite = backupSprite!
        }
        if hex.soldier == nil || hex.controller != player {
            //debug
            let none = "none"
            print("placing \(piece.name) at id: \(hex.id) with doodad: \(hex.doodad == nil ? none : hex.doodad!.name )")
            //end debug
            hex.soldier = piece
            if let sprite = csprite {
                if !animated {
                    sprite.position = hex.background.position
                    sprite.position.y = sprite.position.y + 4
                } else {
                    sprite.runAction(SKAction.moveTo(
                        CGPoint(x: hex.background.position.x, y: hex.background.position.y + 4),
                        duration: 0.1)
                    )
                }
                //print("Sprite position: \(sprite.position)")
                sprite.zPosition = Layer.Occupier
                sprite.size = CGSize(width: 48, height: 32)
                sprite.removeFromParent()
                rootUnitNode.addChild(sprite)
                if let oldSprite = unitMap[hex.id] {
                    oldSprite.removeFromParent()
                }
                unitMap[hex.id] = sprite
                if hex.controller != player {
                    print("Resolving conquest of tile")
                    resolveConquest(piece, player: player, to: &hex)
                }
            }
            if hex.doodad != nil {
                hex.doodad = nil
                hex.moved = true
            }
            return true
        } else if hex.controller == player && hex.soldier!.defense + piece.defense <= 4 {
            csprite!.removeFromParent()
            let defense = hex.soldier!.defense + piece.defense
            print("upgrading piece. new defense \(defense)")
            let type = SoldierType.defaultSoldierTypes[defense-1]
            unitMap[hex.id]?.texture = soldierTextures[type.image]
            hex.soldier = type
            return true
        }
        return false
    }
    
    //pick up the unit from that hex, returns true if we can
    func pickupUnit(hex: Hexagon) -> Bool {
        if hex.soldier != nil {
            if !hex.moved {
                if let sprite = unitMap[hex.id] {
                    print("pickup soldier")
                    sprite.removeFromParent()
                    
                    selectedUnit = true
                    holdUnit(sprite, animated: animationsOn)
                    selectedSoldier = hex.soldier
                    hex.soldier = nil
                    unitMap[hex.id] = nil
                    return true
                }
            } else {
                //we can't pick it up, abort
                touchedHex = nil
            }
        }
        return false
    }
        
    override func didMoveToView(view: SKView) { 
        print("moved to view")
        backgroundColor = SKColor(red:0.184, green:0.443, blue:0.776, alpha:1.0)
        // Load settings
        animationsOn = NSUserDefaults.standardUserDefaults().boolForKey(AnimationKey)
        soundOn = NSUserDefaults.standardUserDefaults().boolForKey(SoundKey)
    }
    
    func clearSelection() {
        handSprite.removeFromParent()
        selectedSprite = nil
        selectedSoldier = nil
        selectedUnit = false
    }
    
    func playErrorSound() {
        if soundOn {
            if let sound = errorSound {
                if !sound.playing {
                    sound.prepareToPlay()
                    sound.play()
                }
            }
        }
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        //We touched a GUI button
        for touch in touches {
            let touchedHex = rootTileNode.nodeAtPoint(touch.locationInNode(rootTileNode)) as? SKSpriteNode
            if let hex = touchedHex { //highlight that hex
                self.touchedHex = hex
            }
            let touchedButton = nodeAtPoint(touch.locationInNode(self)) as? SKSpriteNode
            if let button = touchedButton {
                if button.zPosition == Layer.BuyPawn {
                    buyPawnPressed()
                } else if button.zPosition == Layer.BuyFort {
                    buyFortressPressed()
                } else if button.zPosition == Layer.EndTurn {
                    endTurnPressed()
                } else if button.zPosition == Layer.ExitButton {
                    returnToMain()
                }
            }
        }
        
        //We touched a hex
        if let currentHex = touchedHex {
            let hexId = currentHex.userData!["id"] as! Int
            if let hex = hexagons[hexId] {
                if selectedUnit {
                    if let piece = selectedSoldier as? SoldierType {
                        if placePiece(hex, piece: piece, blob: selectedBlob!, animated: animationsOn) {
                            shadeTiles(selectedBlob!.neighbors)
                            clearSelection()
                        } else {
                            playErrorSound()
                            //play error sound
                        }
                    } else if let piece = selectedSoldier as? BuildingType {
                        if placeBuilding(hex, building: piece, blob: selectedBlob!, animated: animationsOn) {
                            handSprite.removeFromParent()
                            clearSelection()
                        } else {
                            playErrorSound()
                            //play error sound
                        }
                    }
                } else if hex.controller == Player.Human && hex.soldier != nil {
                    selectedUnit = pickupUnit(hex)
                    selectedBlob = hex.blob
                } else if !selectedUnit && hex.controller == Player.Human {
                    //if we aren't holding a unit, then select the blob of that hax
                    selectedBlob = hex.blob
                }
            }
        }
        
        // If we're holding a piece, draw it's movement range
        if let piece = selectedSoldier as? SoldierType {
            if let blob = selectedBlob {
                for tile in blob.neighbors {
                    //print("Selected soldier: \(piece)")
                    if canOverpowerDefense(tile, piece: piece) {
                        //print("Can overpower hex #\(tile.id)")
                        tile.background.color = SKColor.redColor()
                        tile.background.colorBlendFactor = 0.75
                    }
                }
            }
        }
        
        // Up date the income labels
        if let blob = selectedBlob {
            var gold = 0
            if let hut = blob.hut {
                gold = hut.gold
            }
            vaultLabel.text = String(gold)
            let netIncome = blob.netIncome()
            var color = SKColor.blackColor()
            var symbol = "+"
            if netIncome < 0 {
                color = SKColor.redColor()
                symbol = "-"
            } else if netIncome > 0 {
                color = SKColor.greenColor()
            }
            let text = "\(symbol) \(abs(netIncome))"
            profitLabel.text = text
            profitLabel.fontColor = color
        } else {
            vaultLabel.text = ""
            profitLabel.text = ""
        }
    }
    
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
        let scaleMin : CGFloat = 0.8
        let scaleMax : CGFloat = 1.2
        
        // go through each hex
        for hex in hexagons {
            if let h = hex {
                // if it is a human piece that is a soldier and hasn't move
                if h.controller == Player.Human && h.soldier != nil && !h.moved {
                    if let unit = unitMap[h.id] {
                        //then make it get bigger and smaller
                        h.scale += (h.scaleChange)
                        if h.scale <= scaleMin || h.scale >= scaleMax {
                            h.scaleChange = -h.scaleChange
                        }
                        unit.runAction(SKAction.scaleTo(h.scale, duration: 0))
                    }
                }
            }
        }
    }
}
