//
//  MapData.swift
//  Slayer
//
//  Created by jon on 12/16/15.
//  Copyright © 2015 Havron, Jon. All rights reserved.
//

import Foundation
import SpriteKit

class MapData {
    
    
    //Mark - Map Information.
    static let defaultWidth = 9
    static let defaultHeight = 44
    static let defaultMap : [Int] = [
        0,0,0,0,1,1,0,0,0,     //this format is terrible, hard to see if they have neighbors
        0,0,0,0,1,0,0,0,0,
        0,0,0,0,1,1,0,0,0,
        0,0,1,1,1,1,0,0,0,
        0,0,0,1,1,1,0,0,0,
        0,0,0,0,1,0,0,0,0,
        0,0,0,0,1,0,0,0,0,
        0,0,1,1,1,1,1,0,0,
        0,0,0,1,1,1,1,0,0,
        0,0,0,0,1,0,0,0,0,
        0,0,0,0,1,0,0,0,0,
        0,0,1,1,1,1,0,0,0,
        0,0,0,1,1,1,0,0,0,
        0,0,0,0,1,0,0,0,0,
        0,0,0,0,1,0,0,0,0,
        0,0,2,2,2,2,2,0,0,
        0,0,0,1,2,2,2,0,0,
        0,0,0,0,2,0,0,0,0,
        0,0,0,0,2,0,0,0,0,
        0,0,3,3,3,1,1,0,0,
        0,0,0,3,3,1,1,0,0,
        0,0,0,0,1,0,0,0,0,
        0,0,0,0,1,0,0,0,0,
        0,0,3,3,1,1,0,0,0,
        0,0,0,3,3,1,0,0,0,
        0,0,0,0,1,0,0,0,0,
        0,0,0,0,1,0,0,0,0,
        0,0,4,4,5,5,5,0,0,
        0,0,0,4,4,5,5,0,0,
        0,0,0,0,5,0,0,0,0,
        0,0,0,0,5,0,0,0,0,
        0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0
    ]
    
    var data : [Int]
    var width : Int
    var height : Int
    var count : Int
    let spacingWidth = 96 //spacing between tiles in pixels
    
    //map extents
    var minX = Int.max
    var maxX = 0
    var minY = Int.max
    var maxY = 0
    
    init(width: Int = MapData.defaultWidth, height: Int = MapData.defaultHeight,
         let data: [Int] = MapData.defaultMap){
        self.height = height
        self.width = width
        self.data = data
        self.count = height * width
    }
    
    func updateMapExtents(x: Int, y: Int) {
        if x < minX {
            minX = x
        }
        if x > maxX {
            maxX = x
        }
        if y < minY {
            minY = y
        }
        if y > maxY {
            maxY = y
        }
    }
    
    var getMapSize : CGSize {
        return CGSize(width: maxX - minX, height: maxY - minY)
    }
    
    func addHexagonRow(let ts: TileScene, mapIndex: Int, startX: Int, startY: Int){
        // record map extents
        
        var x = startX
        for i in 0 ..< width {
            let index = mapIndex + i
            if index < count && data[index] != 0 {
                updateMapExtents(x, y: startY)
                
                let hex = SKSpriteNode(texture: ts.hexTexture)
                hex.position = CGPoint(x:x, y:startY)
                hex.size = CGSize(width: 64.5, height: 48.5)
                hex.zPosition = Layer.Tile
                hex.color = Player.All[data[index]-1].color
                hex.colorBlendFactor = 1
                hex.userData = ["id": index]
                ts.rootTileNode.addChild(hex)
                ts.hexMap[index] = hex
                let hexagon = Hexagon(id: index, background: hex)
                hexagon.controller = Player.All[data[index]-1]
                ts.hexagons.append(hexagon)
            } else {
                ts.hexagons.append(nil)
            }
            x += spacingWidth
        }
    }
    
    func addHexagons(let ts: TileScene) {
        for _ in 1 ... count {
            ts.hexMap.append(nil)
            ts.unitMap.append(nil)
        }
        
        //draw the hex map borders
        var stepX = 48 //width of a hexagon in pixels
        let stepY = 24 //spacing between rows in pixels
        
        var x = 0
        var y = 0
        var mapIndex = 0
        var rows = 0
        while(mapIndex < count){
            addHexagonRow(ts, mapIndex: mapIndex, startX: x, startY: y)
            x += stepX
            y += stepY
            stepX = -stepX
            mapIndex += width
            rows++
        }
        
        GameSetup.linkNeighbors(&ts.hexagons, mapWidth: width, mapHeight: height)
        GameSetup.createBlobs(ts.hexagons)
        GameSetup.placeHuts(ts)
        GameSetup.placeTrees(ts)
        
        ts.addChild(ts.rootTileNode)
        ts.addChild(ts.rootUnitNode)
        print("Map has \(rows) rows")
    }
}