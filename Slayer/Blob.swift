//
//  Blob.swift
//  Slayer
//
//  Created by Havron, Jonathan Michael on 12/8/15.
//  Copyright © 2015 Havron, Jon. All rights reserved.
//

import Foundation
import SpriteKit

func ==(lhs: Blob, rhs: Blob) -> Bool{
    return lhs.id == rhs.id
}

// a set of tiles for determining movement and income
class Blob : Equatable {
    static var count = 0
    var tiles : Set<Hexagon>
    var id : Int
    var neighbors : Set<Hexagon>
    var player : Player
    var hut    : Hexagon?
    
    var count : Int {
        get {
            return tiles.count
        }
    }
    
    init(let player: Player) {
        self.tiles = Set<Hexagon>()
        self.neighbors = Set<Hexagon>()
        self.id = Blob.count
        Blob.count++
        self.player = player
    }
    
    func addNeighbor(let tile: Hexagon) {
        neighbors.insert(tile)
    }
    
    func addTile(let tile: Hexagon) {
        tiles.insert(tile)
        //tile.blob = self
    }
    
    //why ever use this?
    //just recreate the blobs each move 
    func removeTile(tile: Hexagon) {
        tiles.remove(tile)
        tile.blob = nil
    }
    
    func placeHut() -> Hexagon? {
        let count = tiles.count
        assert(count > 1)
        //choose random tile within the set and give it the hut
        var index = uniformRandom(count)
        //print("Blob has \(count) hexes. Rolled \(index).")
        for (i,t) in tiles.enumerate() {
            if i == index {
                if t.isEmpty() {
                    hut = t
                    t.building = BuildingType.hut
                    break
                } else {
                    index++
                }
            }
        }
        //print("Index ended at \(index)")
        if hut == nil {
            print("huh ran out of huts")
            for t in tiles {
                if let b = t.building {
                    if b == BuildingType.hut {
                        hut = t
                    }
                }
            }
        } 
        return hut
    }
    
    func contains(let tile: Hexagon) -> Bool {
        return tiles.contains(tile)
    }
    
    func canMove(let tile: Hexagon) -> Bool {
        if tile.blob != nil && tile.blob! == self {
            return true
        } else {
            // go through each neighbor
            for neighbor in tile.neighbors {
                // if the tile's neighbor is in my blob, then we can move there
                if neighbor != nil && neighbor!.blob == self {
                    return true
                }
            }
        }
        return false
    }
    
    func getUpkeep() -> Int {
        var total = 0
        var soldiers = 0
        for tile in tiles {
            if let s = tile.soldier {
                total += s.upkeep
                soldiers++
            }
        }
        print("Blob \(id)(\(player.name)) has \(soldiers) soldiers.")
        return total
    }
    
    func getIncome() -> Int {
        var total = 0
        for tile in tiles {
            total += tile.getIncome()
        }
        return total
    }
    
    func netIncome() -> Int {
        let income = getIncome()
        let upkeep = getUpkeep()
        //print("Blob \(id)(\(player.name)) has upkeep \(upkeep)/\(income)")
        return income - upkeep
    }
}
