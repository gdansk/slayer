//
//  BuildingType.swift
//  Slayer
//
//  Created by jon on 11/7/15.
//  Copyright © 2015 Havron, Jon. All rights reserved.
//

import Foundation

func ==(lhs: BuildingType, rhs: BuildingType) -> Bool {
    return lhs.id == rhs.id
}

class BuildingType : CanDefend {
    // MARK: - Properties
    //maybe a delegate to handle wether it can be placed or not?
    var placementDelegate : NSObject?
    var name : String = "Unknown Building"
    var defense : Int { return btDefense }
    var cost : Int = -1
    var image: String = "Hut"
    let id   : Int
    
    // MARK: - Private
    var btDefense : Int = -1
    
    // MARK: - Initializers
    init(id: Int, name: String, defense: Int, cost: Int, image: String = "Hut"){
        self.name = name
        self.cost = cost
        self.btDefense = defense
        self.image = image
        self.id = id
    }
    
    // MARK: - Static
    static let defaultBuildingTypes = [
        BuildingType(id: 0, name: "Hut", defense: 1, cost: -1), //you can't build these
        BuildingType(id: 1, name: "Fort", defense: 2, cost: 15, image: "Fort"),
    ]
    
    static var hut  = BuildingType.defaultBuildingTypes[0]
    static var fort = BuildingType.defaultBuildingTypes[1]
}